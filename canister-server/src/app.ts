import * as express from "express";

import bodyParser = require('body-parser');

import token = require('./middleware/token');

import images = require('./routes/images');
import users = require('./routes/users');

let app = express();
let multer = require('multer');

let upload = multer({
    dest: 'uploads/'
});

// allows us to parse body attributes
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// user routes
app.post('/api/users', users.create);
app.post('/api/users/login', users.login);

// image routes
app.post('/api/images', token.authenticateToken, upload.single('image'), images.create);
app.get('/api/images/:id', images.get);

app.listen(8085, () => {
    console.log("Started at 8085");
});
