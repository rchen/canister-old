import express = require('express');

import User = require('../models/user');

/**
 * Authenticates a token, then attaches the user to the request object to be passed on to the next function if valid.
 */
export function authenticateToken(req: express.Request, res: express.Response, next: Function) {
  let authorizationHeader = req.get('Authorization');
  if (!authorizationHeader) {
    return res.sendStatus(401);
  }

  let tokenHeader = authorizationHeader.split(' ');

  let method = tokenHeader[0];
  let token = tokenHeader[1];

  User.validateToken(token, (user: User) => {
    if (user) {
      req.user = user;
      next();
    } else {
      return res.sendStatus(401);
    }
  });
}
