import * as express from 'express';

import User = require('../models/user');

export function create(req: express.Request, res: express.Response) {
  let email = req.body.email;
  let password = req.body.password;

  let user = new User(email, password);

  return res.json({
    success: user.save()
  });
}

export function login(req: express.Request, res: express.Response) {
  let email = req.body.email;
  let password = req.body.password;

  User.login(email, password, (token: string) => {
    res.json({
      token: token
    });
  });
}
