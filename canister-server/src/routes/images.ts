import * as express from 'express';

import uuid = require('node-uuid');

import Image = require('../models/image');
import User = require('../models/user');

export function create(req: express.Request, res: express.Response) {
  let user: User = req.user;
  let image = new Image(req.file.path, user.id);

  image.rebuildSizes(() => {
    res.json(image);
  });
}

export function get(req: express.Request, res: express.Response) {
  let id = req.params.id;

  Image.get(id, (image) => {
    res.sendFile(image.path + '/lg.jpg', { root: '.' });
  });
}
