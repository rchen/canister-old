import bcrypt = require('bcrypt');
import uuid = require('node-uuid');

import db = require('../config/db');

class User {
  public static TABLE = 'users';
  private static HASH_ROUNDS = 10;

  public id: number;
  private email: string;
  private password: string;
  private token: string;

  constructor(email: string, password: string) {
    this.email = email;
    this.password = bcrypt.hashSync(password, User.HASH_ROUNDS);
    this.token = uuid.v4();
  }

  public static login(email: string, password: string, cb: Function) {
    db.get('SELECT * FROM users WHERE email = $email', {
      $email: email
    }, (err: Error, row: User) => {
      if (row && bcrypt.compareSync(password, row.password)) {
        cb(row.token);
      } else {
        cb(null);
      }
    });
  }

  public static validateToken(token: string, cb: Function) {
    db.get('SELECT * FROM users WHERE token = $token', {
      $token: token
    }, (err: Error, row: User) => {
      cb(row);
    });
  }

  public save() {
    db.get('SELECT * FROM users WHERE email = $email', {
      $email: this.email
    }, (err: Error, row: User) => {
      if (!row) {
        db.run('INSERT INTO users VALUES (null, ?, ?, ?)', this.email, this.password, this.token);
        return true;
      }
      return false;
    });
  }
}

export = User;
