import db = require('../config/db');
import fs = require('fs');
import gm = require('gm');
import mkdirp = require('mkdirp');
import uuid = require('node-uuid');

class Image {
  public static TABLE = 'images';

  private static SMALL_MAX = 140;
  private static MEDIUM_MAX = 640;
  private static LARGE_MAX = 1600;

  public path: string;
  private user_id: number;
  private uuid: string;

  /**
   * Creates an Image instance from a path string.
   *
   * @param {string} Current relative path
   */
  constructor(path: string, user_id: number) {
    this.path = path;
    this.user_id = user_id;
    this.uuid = uuid.v4();
  }

  public rebuildSizes(cb: Function) {
    gm(this.path).size((err, size) => {
      let originalPath = this.path;
      if (!err) {
        let isHorizontal = size.width > size.height ? true : false;
        if (isHorizontal) {
          let path = './uploads/' + this.uuid + '/';
          mkdirp(path, (err) => {
            // image doesn't upload to the correct spot
            // we move it around in the filesystem and also generate the other sizes for it
            gm(originalPath).resize(Image.SMALL_MAX).write(path + 'sm.jpg', () => {});
            gm(originalPath).resize(Image.MEDIUM_MAX).write(path + 'md.jpg', () => {});
            gm(originalPath).resize(Image.LARGE_MAX).write(path + 'lg.jpg', () => {});
            gm(originalPath).write(path+'og.jpg', () => {});
            fs.unlinkSync(originalPath);
            this.path = path;
            db.run('INSERT INTO images VALUES (?, ?, ?)', this.uuid, this.path, this.user_id);
            cb();
          });
        }
      }
    });
  }

  public static get(id: string, cb: (image: Image) => void) {
    db.get('SELECT * FROM images WHERE id = $id', {
      $id: id
    }, (err: Error, row: Image) => {
      cb(row);
    });
  }
}

export = Image;
