var concat = require('gulp-concat'),
    del = require('del'),
    gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    rs = require('run-sequence'),
    ts = require('gulp-typescript');

var tsProject = ts.createProject('tsconfig.json');

gulp.task('build', ['clean'], function() {
    return tsProject.src()
        .pipe(ts(tsProject))
        .js
        .pipe(gulp.dest('./build'));
});

gulp.task('clean', function(cb) {
    del(['./build'], cb);
});

gulp.task('default', function(cb) {
    rs('build',
       'watch',
       'serve');
});

gulp.task('serve', function() {
    nodemon({
        script: './build/src/app.js',
        ext: 'js html',
        env: {
            'NODE_ENV': 'development'
        }
    });
});

gulp.task('watch', function() {
    gulp.watch(['./src/**/*'], ['build']);
});
