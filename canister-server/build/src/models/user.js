var bcrypt = require('bcrypt');
var uuid = require('node-uuid');
var db = require('../config/db');
var User = (function () {
    function User(email, password) {
        this.email = email;
        this.password = bcrypt.hashSync(password, User.HASH_ROUNDS);
        this.token = uuid.v4();
    }
    User.login = function (email, password, cb) {
        db.get('SELECT * FROM users WHERE email = $email', {
            $email: email
        }, function (err, row) {
            if (row && bcrypt.compareSync(password, row.password)) {
                cb(row.token);
            }
            else {
                cb(null);
            }
        });
    };
    User.validateToken = function (token, cb) {
        db.get('SELECT * FROM users WHERE token = $token', {
            $token: token
        }, function (err, row) {
            cb(row);
        });
    };
    User.prototype.save = function () {
        var _this = this;
        db.get('SELECT * FROM users WHERE email = $email', {
            $email: this.email
        }, function (err, row) {
            if (!row) {
                db.run('INSERT INTO users VALUES (null, ?, ?, ?)', _this.email, _this.password, _this.token);
                return true;
            }
            return false;
        });
    };
    User.TABLE = 'users';
    User.HASH_ROUNDS = 10;
    return User;
})();
module.exports = User;
