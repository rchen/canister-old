var db = require('../config/db');
var fs = require('fs');
var gm = require('gm');
var mkdirp = require('mkdirp');
var uuid = require('node-uuid');
var Image = (function () {
    function Image(path, user_id) {
        this.path = path;
        this.user_id = user_id;
        this.uuid = uuid.v4();
    }
    Image.prototype.rebuildSizes = function (cb) {
        var _this = this;
        gm(this.path).size(function (err, size) {
            var originalPath = _this.path;
            if (!err) {
                var isHorizontal = size.width > size.height ? true : false;
                if (isHorizontal) {
                    var path = './uploads/' + _this.uuid + '/';
                    mkdirp(path, function (err) {
                        gm(originalPath).resize(Image.SMALL_MAX).write(path + 'sm.jpg', function () { });
                        gm(originalPath).resize(Image.MEDIUM_MAX).write(path + 'md.jpg', function () { });
                        gm(originalPath).resize(Image.LARGE_MAX).write(path + 'lg.jpg', function () { });
                        gm(originalPath).write(path + 'og.jpg', function () { });
                        fs.unlinkSync(originalPath);
                        _this.path = path;
                        db.run('INSERT INTO images VALUES (?, ?, ?)', _this.uuid, _this.path, _this.user_id);
                        cb();
                    });
                }
            }
        });
    };
    Image.get = function (id, cb) {
        db.get('SELECT * FROM images WHERE id = $id', {
            $id: id
        }, function (err, row) {
            cb(row);
        });
    };
    Image.TABLE = 'images';
    Image.SMALL_MAX = 140;
    Image.MEDIUM_MAX = 640;
    Image.LARGE_MAX = 1600;
    return Image;
})();
module.exports = Image;
