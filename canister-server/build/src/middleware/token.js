var User = require('../models/user');
function authenticateToken(req, res, next) {
    var authorizationHeader = req.get('Authorization');
    if (!authorizationHeader) {
        return res.sendStatus(401);
    }
    var tokenHeader = authorizationHeader.split(' ');
    var method = tokenHeader[0];
    var token = tokenHeader[1];
    User.validateToken(token, function (user) {
        if (user) {
            req.user = user;
            next();
        }
        else {
            return res.sendStatus(401);
        }
    });
}
exports.authenticateToken = authenticateToken;
