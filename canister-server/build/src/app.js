var express = require("express");
var bodyParser = require('body-parser');
var token = require('./middleware/token');
var images = require('./routes/images');
var users = require('./routes/users');
var app = express();
var multer = require('multer');
var upload = multer({
    dest: 'uploads/'
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.post('/api/users', users.create);
app.post('/api/users/login', users.login);
app.post('/api/images', token.authenticateToken, upload.single('image'), images.create);
app.get('/api/images/:id', images.get);
app.listen(8085, function () {
    console.log("Started at 8085");
});
