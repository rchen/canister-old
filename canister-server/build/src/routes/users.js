var User = require('../models/user');
function create(req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var user = new User(email, password);
    return res.json({
        success: user.save()
    });
}
exports.create = create;
function login(req, res) {
    var email = req.body.email;
    var password = req.body.password;
    User.login(email, password, function (token) {
        res.json({
            token: token
        });
    });
}
exports.login = login;
