var Image = require('../models/image');
function create(req, res) {
    var user = req.user;
    var image = new Image(req.file.path, user.id);
    image.rebuildSizes(function () {
        res.json(image);
    });
}
exports.create = create;
function get(req, res) {
    var id = req.params.id;
    Image.get(id, function (image) {
        res.sendFile(image.path + '/lg.jpg', { root: '.' });
    });
}
exports.get = get;
