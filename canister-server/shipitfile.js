module.exports = function(shipit) {
  require('shipit-deploy')(shipit);

  shipit.initConfig({
    default: {
      deployTo: '/var/www/canister.rogr.me',
      key: '/home/rogerchen/.ssh/id_rsa',
      repositoryUrl: 'https://rchen@bitbucket.org/rchen/canister.git',
      workspace: '/tmp/canister.rogr.me'
    },
    production: {
      servers: 'roger@canister.rogr.me'
    }
  });
}
